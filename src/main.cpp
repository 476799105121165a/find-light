#include <Arduino.h>
#define SENSORS_COUNT 5
#define BUTTON_PIN 19
#define LEARN_READY_LED_PIN 22
#define LEARN_MODE_LED_PIN 21


int sensorPins[] = {26, 27, 32, 34, 35};
float sensorValues[SENSORS_COUNT];
long lastTimeReadSensors = 0;

// Массив, содержащий значения сенсоров в темноте
int sensorDarkValues[SENSORS_COUNT];


// Массив, содержащий значения сенсоров на ярком свете
int sensorLightValues[SENSORS_COUNT];

// Режим обучения. 0 - нет обучения, 1 - обучаем режиму НЕТ СВЕТА, 2 - обучаем режиму САМЫЙ ЯРКИЙ СВЕТ
int learnMode = 0;

// ПРедыдущее состояние кнопки. Используется для функции детектирования нажатия на кнопку
int buttonState = LOW;

// Флаг указывает, что было нажатие кнопки
bool buttonPressed = false;

void setup() {
    Serial.begin(115200);
    for (int index = 0; index < SENSORS_COUNT; index++) {
        pinMode(sensorPins[index], INPUT);
    }
    // Регистрируем пин для кнопки
    pinMode(BUTTON_PIN, INPUT_PULLDOWN);
    // регистрируем пины для светодиодов
    pinMode(LEARN_READY_LED_PIN, OUTPUT);
    pinMode(LEARN_MODE_LED_PIN, OUTPUT);
}
//----------------------------------------------------------------

// Включает светодиоды согласно текущему режиму обучения
void setLearnLedStates(){
    switch (learnMode){
        case 0:{
            // Нет обучения. Выключаем все светодиоды
            digitalWrite(LEARN_READY_LED_PIN, LOW);
            digitalWrite(LEARN_MODE_LED_PIN, LOW);
            break;
        }
        case 1:{
            // обучение ТЕМНОМУ режиму
            digitalWrite(LEARN_READY_LED_PIN, HIGH);
            digitalWrite(LEARN_MODE_LED_PIN, LOW);
            break;
        }
        case 2: {
            // Обучение максимальной яркости
            digitalWrite(LEARN_READY_LED_PIN, HIGH);
            digitalWrite(LEARN_MODE_LED_PIN, HIGH);
            break;
        }
    }
}
//----------------------------------------------------------------

void handleButton() {
    // Читаем состояние кнопки
    int currentButtonState = digitalRead(BUTTON_PIN);

    if ((buttonState == LOW) && (currentButtonState == HIGH)) {
        // Кнопка нажата. Ее состояние перешло от LOW к HIGH
        buttonState = HIGH;
        buttonPressed = true;
        // гашение дребезга кнопки
        delay(50);
    } else if ((buttonState == HIGH) && (currentButtonState == LOW)) {
        // Кнопка отпущена. Ее состояние перешло от HIGH к LOW
        buttonState = LOW;
        // гашение дребезга кнопки
        delay(50);
    }
}
//----------------------------------------------------------------

/**
 * считываем яркость с датчика , < в процентах
 */

void readSensors(){
    for (int index = 0; index < SENSORS_COUNT; index++){
        float val = analogRead(sensorPins[index]);
        sensorValues[index] = (val - sensorDarkValues[index]) / (sensorLightValues[index] - sensorDarkValues[index]);
    }
}

void analyseSensors() {
    // Обеспечиваем паузу между чтениями в 200 мс
    if (lastTimeReadSensors < (millis() - 200)) {
        lastTimeReadSensors = millis();

        readSensors();

        // Первое значение в массиве принимаем за максимальное
        float maxValue = sensorValues[0];
        // так же запоминаем индекс найденного максимального значения
        int indexOfMaxValue = 0;

        // Проходим по массиву и сравниваем
        for (int index = 1; index < SENSORS_COUNT; index++) {
            // имеющееся у на максимальное значение с значением следующего элемента массива
            if (maxValue < sensorValues[index]) {
                // найден элемент больший, чем ранее найденный
                // запоминаем значение
                maxValue = sensorValues[index];
                // запоминаем индекс нового максимального значения
                indexOfMaxValue = index;
            }
        }

        // пробежим по массиву значений сеносоров и сбросим ВСЕ значения в ноль
        for (int index = 0; index < SENSORS_COUNT; index++) {
            sensorValues[index] = 0;
        }
        // элемент значений сенсоров с индексом, равным индексу максимального значения ставим в 1
        sensorValues[indexOfMaxValue] = 1;

        // Выведем на экран наш массив, который теперь содержит нули для всех
        // датчиков, кроме максимально, который содержит 1
        for (int index = 0; index < SENSORS_COUNT; index++) {
            Serial.print(String((int)sensorValues[index]) + "   ");
        }
        // Переведем на новую строку
        Serial.println();
    }

}
//----------------------------------------------------------------

/*
 * Обучение полной темноте
 */
void learnDark(){
    // Считываем темновые показания датчиков в массив для темновых показаний
    for (int index = 0; index < SENSORS_COUNT; index++){
        sensorDarkValues[index]
                = analogRead(sensorPins[index]);
    }

    // Вывод на экран
    Serial.println("");
    for (int index = 0; index < SENSORS_COUNT; index++){
        Serial.println(String(index) + " = " + String(sensorDarkValues[index]));
    }
//    for (int index = 0; index < SENSORS_COUNT; sensorDarkValues[index] = analogRead(sensorPins[index++]));
}
//----------------------------------------------------------------

/*
 * Обучение полному свету
 */
void learnBright(){
    // Считываем темновые показания датчиков в массив для темновых показаний
    for (int index = 0; index < SENSORS_COUNT; index++){
        sensorLightValues[index]
                = analogRead(sensorPins[index]);
    }

    // Вывод на экран
    Serial.println("");
    for (int index = 0; index < SENSORS_COUNT; index++){
        Serial.println(String(index) + " = " + String(sensorLightValues[index]));
    }
//    for (int index = 0; index < SENSORS_COUNT; sensorDarkValues[index] = analogRead(sensorPins[index++]));
}
//----------------------------------------------------------------







void loop() {
    // Читаем состояние кнопки
    handleButton();

    // Проверяем была ли нажата кнопка
    if (buttonPressed) {
        // Сброс признака, что кнопка была нажата
        buttonPressed = false;

        // Если режим обучения ТЕМНОТА переходит в режим обучения
        // СВЕТ, то читаем состояние затемненных датчиков
        if (learnMode == 1){
            learnDark();
        }else if (learnMode == 2){
            learnBright();
        }

        // Переключаем режимы обучения
        learnMode++;
        // Если режим перешел ЗА режим обучения свету, то переходим в режим НЕТ ОБУЧЕНИЯ
        if (learnMode > 2){
            learnMode = 0;
        }
        setLearnLedStates();
    }
if (learnMode == 0) {
    analyseSensors();
}

}







// УЧИМСЯ   ТЕМНО / СВЕТЛО
//   1           0

// dark     light    считали только что (value)      приведенное значение яркости
// 100   -  1000        450
// 50    -  600       450

//  scale = light - dark = 1000 - 100 = 900
// % = value * 100% / scale = 450 * 100% / 900 = 50%

// scale = 600 - 50 = 550
// % = 450 * 100% / 550 = 81.8%







